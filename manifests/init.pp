class lysgraf {
  class {'::prometheus':
    web_listen_address => '127.0.0.1:9090',
  }

  class {'::prometheus::server':
    version           => '2.31.1',
    storage_retention => '3650d',
    scrape_configs    => [
      {
        'job_name'        => 'ceph-mgr',
        'scrape_interval' => '10s',
        'scrape_timeout'  => '10s',
        'static_configs'  => [
          {
            'targets' => [
              'vogon-0:9283',
              'vogon-1:9283',
              'vogon-2:9283',
            ],
            'labels'  => {
              'alias' => 'Ceph',
            }
          },
        ],
      },
      {
        'job_name'        => 'node_exporter_ceph',
        'scrape_interval' => '10s',
        'scrape_timeout'  => '10s',
        'static_configs'  => [
          {
            'targets' => [
              'vogon-0:9100',
              'vogon-1:9100',
              'vogon-2:9100',
              'trillian-0:9100',
              'trillian-1:9100',
              'trillian-2:9100',
              'trillian-3:9100',
              'trillian-4:9100',
              'trillian-5:9100',
              'trillian-6:9100',
              'trillian-7:9100',
              'babelfish:9100',
              'infinidim-0:9100',
            ],
          },
        ],
      },
      {
        'job_name'        => 'node_exporter_services',
        'scrape_interval' => '10s',
        'scrape_timeout'  => '10s',
        'static_configs'  => [
          {
            'targets' => [
              'graf:9100',
              'diskmaskin:9100',
              'studieinfo:9100',
              'orwell:9100',
              'nyaa:9100',
              'mail:9101',
              'chapman:9100',
              'eon:9100',
            ],
          },
        ],
      },
      {
        'job_name'        => 'node_exporter_ipa',
        'scrape_interval' => '10s',
        'scrape_timeout'  => '10s',
        'static_configs'  => [
          {
            'targets' => [
              'trocca.ad:9100',
              'champis.ad:9100',
              'pommac.ad:9100',
            ],
          },
        ],
      },
      {
        'job_name'        => 'node_exporter_ftp',
        'scrape_interval' => '10s',
        'scrape_timeout'  => '10s',
        'static_configs'  => [
          {
            'targets' => [
              'hina:9100',
              'anzu:9100',
              'anzu:9192',
              'gripen:9100',
              'laban:9100',
              'labolina:9100',
              'anna:9100',
              'nike:9100',
              'kukuri:9100',
            ],
          },
        ],
      },
      {
        'job_name'        => 'node_exporter_milliways',
        'scrape_interval' => '10s',
        'scrape_timeout'  => '10s',
        'static_configs'  => [
          {
            'targets' => [
              'milliways-0:9100',
              'milliways-1:9100',
              'milliways-2:9100',
              'milliways-3:9100',
              'milliways-4:9100',
              'milliways-5:9100',
              'milliways-6:9100',
              'milliways-7:9100',
              'milliways-8:9100',
            ],
          },
        ],
      },
      {
        'job_name'        => 'ftp_haproxy',
        'scrape_interval' => '10s',
        'scrape_timeout'  => '10s',
        'static_configs'  => [
          {
            'targets' => [
              'laban:9101',
              'labolina:9101',
              'anna:9101',
            ],
          },
        ],
      },
      {
        'job_name'        => 'ftp_varnish',
        'scrape_interval' => '10s',
        'scrape_timeout'  => '10s',
        'static_configs'  => [
          {
            'targets' => [
              'laban:9131',
              'labolina:9131',
              'anna:9131',
            ],
          },
        ],
      },
      {
        'job_name'        => 'node_exporter_inhysningar',
        'scrape_interval' => '10s',
        'scrape_timeout'  => '10s',
        'static_configs'  => [
          {
            'targets' => [
              'holgerspexet:9100',
              'holgerspexet-public:9100',
            ],
          },
        ],
      },
      {
        'job_name'        => 'node_exporter_workstations',
        'scrape_interval' => '10s',
        'scrape_timeout'  => '10s',
        'static_configs'  => [
          {
            'targets' => [
              'artemis:9100',
              'borsalino:9100',
              'claptrap:9100',
              'eris:9100',
              'hades:9100',
              'hakumei:9100',
              'lain:9100',
              'mikochi:9100',
              'renge:9100',
            ],
          },
        ],
      },
      {
        'job_name'        => 'services_exporters',
        'scrape_interval' => '10s',
        'scrape_timeout'  => '10s',
        'static_configs'  => [
          {
            'targets' => [
              'hina:9113',
            ],
          },
        ],
      },
      {
        'job_name'       => 'nginx_vts',
        'metrics_path'   => '/metrics/format/prometheus',
        'static_configs' => [
          {
            'targets' => [
              'hina:80',
              'anzu:80',
            ],
          },
        ],
      },
      {
        'job_name'        => 'snmp',
        'metrics_path'    => '/snmp',
        'scrape_interval' => '15s',
        'scrape_timeout'  => '10s',
        'params'          => {
          module => ['if_mib'],
        },
        'static_configs'  => [
          {
            'targets' => [
              'sw-mgmt-bar-r1.mgmt',
              'sw-mgmt-bar-r2.mgmt',
              'sw-mgmt-bar-r3.mgmt',
              'sw-mgmt-bar-r4.mgmt',
              'sw-mgmt-bar-r5.mgmt',
              'sw-mgmt-bar-r7.mgmt',
              'sw-mgmt-bar-r8.mgmt',
              'sw-bar-r2.mgmt',
            ],
          },
        ],
        'relabel_configs' => [
          {
            'source_labels' => ['__address__'],
            'target_label'  => '__param_target',
          },
          {
            'source_labels' => ['__param_target'],
            'target_label'  => 'instance',
          },
          {
            'target_label' => '__address__',
            'replacement'  => 'orwell:9116',
          },
        ],
      },
      # core routers
      {
        'job_name'        => 'snmp_core',
        'metrics_path'    => '/snmp',
        'scrape_interval' => '10s',
        'scrape_timeout'  => '8s',
        'params'          => {
          module => ['if_mib'],
        },
        'static_configs'  => [
          {
            'targets' => [
              'lo-core1.net.lysator.liu.se',
              'lo-core2.net.lysator.liu.se',
            ],
          },
        ],
        'relabel_configs' => [
          {
            'source_labels' => ['__address__'],
            'target_label'  => '__param_target',
          },
          {
            'source_labels' => ['__param_target'],
            'target_label'  => 'instance',
          },
          {
            'target_label' => '__address__',
            'replacement'  => 'orwell:9116',
          },
        ],
      },
      {  # these switches are much slower to poll
        'job_name'        => 'snmp-slow',
        'metrics_path'    => '/snmp',
        'scrape_interval' => '30s',
        'scrape_timeout'  => '25s',
        'params'          => {
          module => ['if_mib'],
        },
        'static_configs'  => [
          {
            'targets' => [
            ],
          },
        ],
        'relabel_configs' => [
          {
            'source_labels' => ['__address__'],
            'target_label'  => '__param_target',
          },
          {
            'source_labels' => ['__param_target'],
            'target_label'  => 'instance',
          },
          {
            'target_label' => '__address__',
            'replacement'  => 'orwell:9116',
          },
        ],
      },
    ],
  }

  class { '::grafana':
    cfg => {
      app_mode             => 'production',
      server               => {
        http_addr => '127.0.0.1',
        http_port => 8080,
        root_url  => "https://${facts['networking']['fqdn']}",
      },
      metrics              => {
        enabled => false,
      },
      'auth.generic_oauth' => {
        enabled              => true,
        name                 => 'Keycloak-OAuth',
        allow_sign_up        => true,
        client_id            => 'grafana-oauth',
        client_secret        => chomp(lookup('lysgraf::grafana::client_secret_content')),
        scopes               => 'openid email profile offline_access roles',
        email_attribute_path => 'email',
        login_attribute_path => 'username',
        name_attribute_path  => 'full_name',
        auth_url             => 'https://login.lysator.liu.se/realms/Lysator/protocol/openid-connect/auth',
        token_url            => 'https://login.lysator.liu.se/realms/Lysator/protocol/openid-connect/token',
        api_url              => 'https://login.lysator.liu.se/realms/Lysator/protocol/openid-connect/userinfo',
        role_attribute_path  => "contains(roles[*], 'admin') && 'Admin' || contains(roles[*], 'editor') && 'Editor' || 'Viewer'",
      },
      smtp                 => {
        enabled      => true,
        from_address => 'graf@graf.lysator.liu.se',
        from_name    => 'Grafana @ Lysator',
        host         => 'mail.lysator.liu.se:25',
      },
    },
  }


  include ::nginx
  nginx::resource::server { $facts['networking']['fqdn']:
    server_name               => [$facts['networking']['fqdn']],
    proxy                     => 'http://127.0.0.1:8080/',

    # Encrypt everything
    ssl_redirect              => true,
    ssl                       => true,
    ssl_cert                  => "/etc/letsencrypt/live/${facts['networking']['fqdn']}/fullchain.pem",
    ssl_key                   => "/etc/letsencrypt/live/${facts['networking']['fqdn']}/privkey.pem",

    # Set the paranoia level to 'high'.
    ssl_protocols             => 'TLSv1.2',
    ssl_ciphers               => 'ECDHE-ECDSA-AES256-GCM-SHA384:ECDHE-RSA-AES256-GCM-SHA384:ECDHE-ECDSA-CHACHA20-POLY1305:ECDHE-RSA-CHACHA20-POLY1305:ECDHE-ECDSA-AES128-GCM-SHA256:ECDHE-RSA-AES128-GCM-SHA256:ECDHE-ECDSA-AES256-SHA384:ECDHE-RSA-AES256-SHA384:ECDHE-ECDSA-AES128-SHA256:ECDHE-RSA-AES128-SHA256',
    ssl_prefer_server_ciphers => 'on',
  }

  class { '::letsencrypt':
    config  => {
      email => 'hx@lysator.liu.se',
    }
  }

  class { '::profiles::letsencrypt':
    certname => $::fqdn,
    provider => 'nginx',
  }

  include ::lysnetwork::iptables

  firewall { '100 accept http(s)':
    proto  => 'tcp',
    jump   => 'accept',
    dport  => ['80','443'],
  }

  systemd::service_limits { 'prometheus.service':
    limits => {
      'LimitNOFILE' => 'infinity',
    }
  }
}
